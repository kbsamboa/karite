Rails.application.routes.draw do
  resources :items
  resources :shelves
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # HOME
  root    'static_pages#home'

  # SESSIONS
  get     '/login',  to: 'sessions#new'
  post    '/login',  to: 'sessions#create'
  delete  '/logout', to: 'sessions#destroy'
end
