# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# require 'bcrypt'
# include BCrypt

# User.delete_all
# User.create(
#   [
#     {
#       name: 'Admin',
#       username: 'admin',
#       password: BCrypt::Engine.hash_secret(
#         'admin123',
#         BCrypt::Engine.generate_salt
#       ),
#       email: 'admin@admin.com',
#       role: 'admin'
#     }
#   ]
# )

# SHELVES
  Shelf.delete_all
  Shelf.create(
    [
      {
        name: 'Shelf_001',
        capacity: '5'
      },
      {
        name: 'Shelf_002',
        capacity: '1'
      },
      {
        name: 'Shelf_003',
        capacity: '10'
      }
    ]
  )

# ITEMS
  Item.delete_all
  Item.create(
    [
      {
        name: 'Abacus',
        shelf_id: '1',
        status: 'SHELVED'
      },
      {
        name: 'Book',
        shelf_id: '1',
        status: 'SHELVED'
      },
      {
        name: 'Comics',
        shelf_id: '1',
        status: 'SHELVED'
      },
      {
        name: 'Drums',
        shelf_id: '1',
        status: 'SHELVED'
      },
      {
        name: 'Envelope',
        shelf_id: '1',
        status: 'SHELVED'
      },
      {
        name: 'Fan',
        shelf_id: '2',
        status: 'SHELVED'
      },
      {
        name: 'Guitar',
        shelf_id: '3',
        status: 'SHELVED'
      },
      {
        name: 'Handkerchief',
        shelf_id: '3',
        status: 'SHELVED'
      },
      {
        name: 'Ice',
        shelf_id: '3',
        status: 'SHELVED'
      },
      {
        name: 'Jellybean',
        shelf_id: '3',
        status: 'SHELVED'
      },
      {
        name: 'Key',
        shelf_id: '3',
        status: 'SHELVED'
      }
    ]
  )
