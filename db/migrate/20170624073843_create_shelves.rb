class CreateShelves < ActiveRecord::Migration[5.1]
  def change
    create_table :shelves do |t|
      t.string :name
      t.integer :capacity

      t.timestamps
    end
  end
end
