class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :name
      t.integer :shelf_id
      t.string :status
      t.integer :borrower_id
      t.date :date_borrowed
      t.date :date_of_return

      t.timestamps
    end
  end
end
