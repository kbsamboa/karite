# README.md
# Created by    : Katrina B. Samboa
# Date Created  : 26 June 2017
# Date Modified : 26 June 2017

DEVELOPMENT ENVIRONMENT
-------------------------------------------------------------------------------
* Virtual Machine (Oracle VM VirtualBox)
** DOWNLOAD LINK: download.virtualbox.org/virtualbox/5.1.22
** FILES DOWNLOADED
    1. VirtualBox-5.1.22-115126-Win.exe
    2. Oracle_VM_VirtualBox_Extension_Pack_5.1.22_115126.vbox.extpack
    3. VBoxGuestAdditions_5.1.22.iso
* Operating System
** VERSION: Ubuntu LTS 16.04
** DOWNLOAD LINK: releases.ubuntu.com/16.04/
** FILE DOWNLOADED: ubuntu-16.04.2-desktop-amd64.iso
* Ruby on Rails
** VERSION
    1. Ruby 2.4.0
    2. Rails 5.1.1
    3. SQLite 1.3.13
** DOWNLOAD LINK: gorails.com/setup/ubuntu/16.04

GOAL
-------------------------------------------------------------------------------
* To create a Lending Inventory System using Ruby on Rails.

REQUIREMENTS
-------------------------------------------------------------------------------
* Create items and shelves.
* Assign an item to a shelf.
* Identify which items were borrowed and when they will be returned,
  and items currently available.
* A user can borrow an item.

DETAILED REQUIREMENTS
-------------------------------------------------------------------------------
* USERS
** 2 types: BORROWER and ADMIN
** ADMIN PRIVILEGES
    1. Create / Update / Delete a different user account (ADMIN / BORROWER)
    2. Create / Update / Delete an item
    3. Create / Delete a shelf
    4. View list of user accounts, items, and shelves
** BORROWER PRIVILEGES
    1. Borrow an item
    2. Return an item
    3. View list of items and their respective details (ALL / BORROWED)
* ITEMS
    1. Can have the following statuses:
      - NO SHELF: Item has not been assigned to a shelf or
                  the chosen shelf is already full
      - SHELVED: Item has been assigned to a shelf
      - BORROWED: Item has been borrowed by a user
      - RETURNED: Item has been returned by a user but is
                  currently not place in its shelf
    2. Cannot be re-assigned to another shelf
* SHELVES
    1. Cannot exceed a maximum of 25 items
    2. Cannot change the capacity value once set

Refer to change.log for other details.