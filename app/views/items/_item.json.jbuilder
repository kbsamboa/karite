json.extract! item, :id, :name, :shelf_id, :status, :borrower_id, :date_borrowed, :date_of_return, :created_at, :updated_at
json.url item_url(item, format: :json)
