json.extract! shelf, :id, :name, :capacity, :created_at, :updated_at
json.url shelf_url(shelf, format: :json)
