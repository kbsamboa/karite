class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]

  def index
    @items = Item.all
  end

  def show
  end

  def new
    @item = Item.new
  end

  def edit
  end

  def create
    @shelf = Shelf.find_by(id: params[:item][:shelf_id])
    @items = Item.where(shelf_id: params[:item][:shelf_id])

    if @items.nil? or (!@shelf.nil? and @items.count < @shelf.capacity)
      params[:item][:status] = 'SHELVED'
    else
      params[:item][:shelf_id] = 0
      params[:item][:status] = 'NO SHELF'
    end

    @item = Item.new(item_params)

    respond_to do |format|
      if @item.save
        format.html { redirect_to @item, notice: 'Item was successfully created.' }
        format.json { render :show, status: :created, location: @item }
      else
        format.html { render :new }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    if @item.status == 'NO SHELF'
      @shelf = Shelf.find_by(id: params[:item][:shelf_id])
      @items = Item.where(shelf_id: params[:item][:shelf_id])
      
      if @items.nil? or (!@shelf.nil? and @items.count < @shelf.capacity)
        params[:item][:status] = 'SHELVED'
      else
        params[:item][:shelf_id] = 0
        params[:item][:status] = 'NO SHELF'
      end
    elsif @item.status == 'SHELVED'
      params[:item][:status] = 'BORROWED'
      params[:item][:date_of_return] = nil
    elsif @item.status == 'BORROWED' and
      !@item.date_borrowed.nil?
      params[:item][:status] = 'SHELVED'
    end

    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { render :edit }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url, notice: 'Item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_item
      @item = Item.find(params[:id])
    end

    def item_params
      params.require(:item).permit(:name, :shelf_id, :status, :borrower_id, :date_borrowed, :date_of_return)
    end
end
